import 'dart:io';

import 'package:dio/dio.dart';

class CusNetworkError {
  static final int noNetwork = -100;
  static final int unKnown = -99;
  static final int cancel = -101;
  static final int sendTimeOut = -102;
  static final int unauthorized = 401;

  static ErrorModel _noNet = ErrorModel(code: noNetwork, message: 'No network connection');
  static ErrorModel _unKnown = ErrorModel(code: unKnown, message: 'An unknown error');
}

class ErrorModel {
  int code;
  String message;

  ErrorModel({this.code, this.message});

  factory ErrorModel.create(Exception e) {

    if(e is DioError) {
      DioError error = e;
      switch (error.type) {
        case DioErrorType.CANCEL:{
          return ErrorModel(code: CusNetworkError.cancel, message: "Request cancelled");
        }
        break;
        case DioErrorType.CONNECT_TIMEOUT:{
          return ErrorModel(code: HttpStatus.networkConnectTimeoutError, message: "Connection timeout");
        }
        break;
        case DioErrorType.SEND_TIMEOUT:{
          return ErrorModel(code: CusNetworkError.sendTimeOut, message: "Request send timeout");
        }
        break;
        case DioErrorType.RECEIVE_TIMEOUT:{
          return ErrorModel(code: HttpStatus.requestTimeout, message: "Response timeout");
        }
        break;
        case DioErrorType.DEFAULT:{
          if (error.response == null) {
            return CusNetworkError._noNet;
          }
          return CusNetworkError._unKnown;
        }
        break;
        case DioErrorType.RESPONSE:{
          try {
            int errCode = error.response.statusCode;
            String errMsg = error.response.statusMessage;
            switch (errCode) {
              case HttpStatus.badRequest: {
                return ErrorModel(code: errCode, message: "请求语法错误");
              }
              break;
              case HttpStatus.forbidden: {
                return ErrorModel(code: errCode, message: "服务器拒绝执行");
              }
              break;
              case HttpStatus.notFound: {
                return ErrorModel(code: errCode, message: "无法连接服务器");
              }
              break;
              case HttpStatus.methodNotAllowed: {
                return ErrorModel(code: errCode, message: "请求方法被禁止");
              }
              break;
              case HttpStatus.internalServerError: {
                return ErrorModel(code: errCode, message: "服务器内部错误");
              }
              break;
              case HttpStatus.badGateway: {
                return ErrorModel(code: errCode, message: "无效的请求");
              }
              break;
              case HttpStatus.serviceUnavailable: {
                return ErrorModel(code: errCode, message: "服务器挂了");
              }
              break;
              case HttpStatus.httpVersionNotSupported: {
                return ErrorModel(code: errCode, message: "不支持HTTP协议请求");
              }
              break;
              case HttpStatus.noContent: {
                return ErrorModel(code: errCode, message: "服务器返回内容为null");
              }
              break;
              default: {
                return ErrorModel(code: errCode, message: errMsg);
              }
            }
          } on Exception catch(_) {
            return CusNetworkError._unKnown;
          }
        }
        break;
        default: {
          return  CusNetworkError._unKnown;
        }
      }
    } else {
      return CusNetworkError._unKnown;
    }
  }
}