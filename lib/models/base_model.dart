abstract class BaseModel<T> {
  // 承载原始数据，主要用于toJson使用
  Map<String, dynamic> _originJson;

  String errorId;
  String errorMsg;
  T data;

  BaseModel({this.errorId, this.errorMsg, this.data});

//  BaseModel get type => this;

  T dataParse(dynamic dataJson);

  Map<String, dynamic> toJson() {
    return _originJson;
  }

  BaseModel fromJson(Map<String, dynamic> json) {
    if (null == json) return null;

    _originJson = json;

    errorId = json['errorId'];
    errorMsg = json['errorMsg'];

    if (json['data'] != null) {
      data = dataParse(json['data']);
    }

    return this;
  }

}