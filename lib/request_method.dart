enum RequestMethod {
  GET,
  POST,
  DELETE,
  PUT,
}

const MethodValues = {
  RequestMethod.GET: "get",
  RequestMethod.POST: "post",
  RequestMethod.DELETE: "delete",
  RequestMethod.PUT: "put"
};