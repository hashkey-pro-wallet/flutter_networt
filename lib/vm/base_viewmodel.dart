import 'dart:async';

abstract class BaseViewModel<T> {
  StreamController _dataSourceController = StreamController<T>.broadcast();

//  Sink get inputData => _dataSourceController;

  Stream get outputData => _dataSourceController.stream;

  addData(T data) {
    _dataSourceController.add(data);
  }

  addError(Object e) {
    _dataSourceController.addError(e);
  }

  dispose() {
    _dataSourceController.close();
  }
}