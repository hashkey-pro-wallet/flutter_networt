abstract class RefreshToken {
  Future<bool> refresh();
}