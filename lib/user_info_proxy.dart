import 'interfaces/user_info.dart';

class UserInfoProxy {
  static UserInfoProxy _instance;

  UserInfoProxy._();

  UserInfoInterface _userInfoImpl;


  static UserInfoProxy getInstance() {
    return _instance ??= UserInfoProxy._();
  }

  void injectUserInfoImpl(UserInfoInterface userInfoImpl) {
    _userInfoImpl = userInfoImpl;
  }

  String getAccessToken() => _userInfoImpl?.getAccessToken();
}