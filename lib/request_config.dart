class RequestConfig {
  String mockPath;
  bool isMock = false;
  double mockDelay = .0;

  bool isShowToast = false;

  RequestConfig({this.isMock, this.mockPath, this.mockDelay, this.isShowToast});

}